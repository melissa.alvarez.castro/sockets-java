package Ejercicio1_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class ProtocolServer {
	public static void procesar(BufferedReader input, PrintWriter output, HashMap<String, Integer> users)
			throws IOException {

		System.out.println("Conectando ...");

		// Lectura del mensaje que el cliente le envia al servidor.
		String clientMessage = input.readLine();

		System.out.println("From client: " + clientMessage);

		String answer = "";

		// Procesamiento de la informacion que el cliente ha enviado
		// al servidor.
		String opcion = clientMessage.split(" ")[0];

		if (opcion.equals("LOGIN")) {
			String name = clientMessage.split(" ")[1];
			if (users.containsKey(name)) {
				users.put(name, users.get(name) + 1);
				answer = "BIENVENIDO, usted es el usuario #" + users.get(name);
			} else {
				users.put(name, 1);
				answer = "ACESSO #" + name;
			}
		} else if (opcion.equals("INFORME")) {
			if(users.size() > 0) {
				try {
					String detalle = clientMessage.split(" ")[1];
					if (detalle.equals("DETALLADO")) {
						for (HashMap.Entry<String, Integer> element : users.entrySet()) {
							answer += element.getKey() + " "+ element.getValue() +", ";
						}
						answer = answer.substring(0, answer.length() - 2);
					}else {
						answer = "El mensaje tiene las siguientes opciones LOGIN nombre, INFORME, INFORME DETALLADO";
					}
				} catch (Exception e) {
					for (HashMap.Entry<String, Integer> element : users.entrySet()) {
						answer += element.getKey() + ", ";
					}
					answer = answer.substring(0, answer.length() - 2);
				}
			}else {
				answer = "Las opciones INFORME, INFORME DETALLADO solo estaran disponibles cuando existan usuarios";
			}
		} else {
			answer = "El mensaje tiene las siguientes opciones LOGIN nombre, INFORME, INFORME DETALLADO";
		}

		System.out.println("Sent to client: " + answer);

		// Envio del mensaje al cliente.
		output.println(answer);
	}
}
