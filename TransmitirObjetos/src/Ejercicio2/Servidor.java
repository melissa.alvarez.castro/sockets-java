package Ejercicio2;

/*
 * EchoTCPServer
 * 
 * El objetivo de este programa es leer de la red un String enviada por un cliente y
 * hacer eco de ese mensaje enviándolo de nuevo al cliente.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class Servidor {
	public static final int PORT = 3400;

	private ServerSocket listener;
	private Socket serverSideSocket;
	private ObjectOutputStream writer;
	private ObjectInputStream reader;
	private HashMap<String, Integer> users;

	public Servidor() throws ClassNotFoundException {
		System.out.println("SERVER ...");

		try {
			listener = new ServerSocket(PORT);

			while (true) {
				System.out.println("The SERVER is waiting for a CLIENT....");
				serverSideSocket = listener.accept();

				try {
					createStreams();
					ProtocolServer.procesar(reader, writer);

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (serverSideSocket != null)
					serverSideSocket.close();
				if (listener != null)
					listener.close();
				if (serverSideSocket != null)
					serverSideSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void createStreams() throws IOException {
		writer = new ObjectOutputStream(serverSideSocket.getOutputStream());
		reader = new ObjectInputStream(serverSideSocket.getInputStream());
	}

	/**
	 * Metodo principal utilizado para lanzar el programa servidor.
	 * @throws ClassNotFoundException 
	 */
	public static void main(String args[]) throws ClassNotFoundException{
		new Servidor();
	}
}