package InterfacesDeRed;

import java.net.NetworkInterface;
import java.net.SocketException;


public class NetworkInterfacesExample02 {
	public static void main(String args[]) {
		try {
			String name = "wlan0";
			NetworkInterface nic = NetworkInterface.getByName(name);
			System.out.println("Interfaz " + nic.getName() + ":");
			byte[] macAddress = nic.getHardwareAddress();
			String m = "";
			int i = 0;
			for (; i < macAddress.length - 1; i++) {
				m += String.format("%02X", macAddress[i]);
				m += "-";
			}
			m += String.format("%02X", macAddress[i]);
			System.out.println("MAC Address para la interfaz " + name + ": " + m);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
}