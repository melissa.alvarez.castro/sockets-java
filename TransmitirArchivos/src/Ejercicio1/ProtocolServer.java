package Ejercicio1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.HashMap;

public class ProtocolServer {
	public static void procesar(ObjectInputStream input, ObjectOutputStream output) throws IOException, ClassNotFoundException {

		System.out.println("Conectando ...");

		Object object = input.readObject();
		System.out.println("Cliente: " + object.toString());
		String answer = "OK";
		output.writeObject(answer);
		output.flush();
	}

}
