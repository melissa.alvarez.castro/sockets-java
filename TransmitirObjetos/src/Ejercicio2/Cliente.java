package Ejercicio2;

/*
 * EchoTCPClient
 * 
 * El objetivo de este programa es leer del teclado un String y enviarlo al
 * servidor. El servidor hace eco de este mensaje y lo envia de nuevo al cliente.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Cliente {
	public static final int PORT = 3400;
	public static final String SERVER = "localhost";

	private Socket clientSideSocket;
	private ObjectOutputStream writer;
	private ObjectInputStream reader;

	public Cliente() throws ClassNotFoundException {
		System.out.println("CLIENT ...");

		try {
			clientSideSocket = new Socket(SERVER, PORT);
			createStreams();
			ProtocolClient.procesar(reader, writer);

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null)
					reader.close();
				if (writer != null)
					writer.close();
				if (clientSideSocket != null)
					clientSideSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void createStreams() throws IOException {
		writer = new ObjectOutputStream(clientSideSocket.getOutputStream());
		reader = new ObjectInputStream(clientSideSocket.getInputStream());
	}

	/**
	 * Metodo principal de la aplicacion.
	 * @throws ClassNotFoundException 
	 */
	public static void main(String args[]) throws ClassNotFoundException {
		new Cliente();
	}
}