package Ejercicio3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class ProtocolServer {
	public static void procesar(BufferedReader input, PrintWriter output, int fase) throws IOException {

		System.out.println("Conectando ...");

		while (fase != 3) {
			String clientMessage = input.readLine();
			System.out.println("From client: " + clientMessage);
			String answer = "";

			switch (fase) {
			case 0:
				System.out.println("Entro fase 0");
				if (clientMessage.equals("Hola")) {
					answer = "Listo";
					fase = 1;
				} else {
					fase = 0;
				}
				break;
			case 1:
				System.out.println("Entro fase 1");
				try {
					int number = Integer.parseInt(clientMessage);
					answer = String.valueOf(number - 1);
					fase = 2;
				} catch (NumberFormatException e) {
					fase = 0;
				}
				break;
			case 2:
				System.out.println("Entro fase 2");
				if (clientMessage.equals("OK")) {
					answer = "ADIOS";
					fase = 3;
				} else {
					fase = 0;
				}

				break;
			default:
				break;
			}
			System.out.println("Sent to client: " + answer);
			output.println(answer);
		}

	}
}