package astarg;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class Protocol {

    public static void procesar(BufferedReader input, PrintWriter output, Map<String, Integer> names) throws IOException {
        System.out.println("Connection incoming ...");

        // Lectura del mensaje que el cliente le envia al servidor.
        String clientMessage = input.readLine();

        System.out.println("From client: " + clientMessage);

        // Procesamiento de la informacion que el cliente ha enviado
        // al servidor.
        String answer = "";
        if(clientMessage.startsWith("LOGIN ")){
            String name = clientMessage.split(" ")[1].toLowerCase();
            if(names.containsKey(name)){
                names.put(name, names.get(name) + 1);
            }else{
                names.put(name, 1);
            }
         if(names.get(name) > 1){
             answer = "Acceso del cliente " + name + " numero " + names.get(name);
         }else{
             answer = "Bienvenido, usted es el usuario " + name;
         }
        }else{
            answer = "El mensaje ingresado por el cliente debe empezar con la palabra LOGIN y un nombre separados por un espacio";
        }


        System.out.println("Sent to client: " + answer);

        // Envio del mensaje al cliente.
        output.println(answer);
    }
}
