package InterfacesDeRed;

import java.net.InetAddress;
import java.net.UnknownHostException;
public class InetAddressExample {
    public static void main(String[] args) {
        System.out.println("Información del host local");
        InetAddress address;
        try {
            address = InetAddress.getLocalHost();
            System.out.println(address);
            System.out.println("Dirección IP   : " + address.getHostAddress());
            System.out.println("Nombre de host : " + address.getHostName());
            System.out.println();
            System.out.println("Dirección IP de www.uniquindio.edu.co");
            address = InetAddress.getByName("www.uniquindio.edu.co");
            System.out.println(address);
            System.out.println();
            System.out.println("Direcciones IP de www.apple.com");
            InetAddress[] addressesArray;
            addressesArray = InetAddress.getAllByName("www.apple.com");
            for (int i = 0; i < addressesArray.length; i++) {
                System.out.println(addressesArray[i]);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}