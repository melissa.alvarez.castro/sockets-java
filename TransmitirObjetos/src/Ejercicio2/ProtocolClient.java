package Ejercicio2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class ProtocolClient {

	public static void procesar(ObjectInputStream input, ObjectOutputStream output)
			throws IOException, ClassNotFoundException {
		Persona persona = new Persona("Melissa", 21, 1.56);
		output.writeObject(persona);
		output.flush();
		Object object = input.readObject();
		System.out.println("Servidor: " + object);
	}

}
