package back;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Protocol {

	public static void procesar(BufferedReader input, PrintWriter output) throws IOException {
		
		
		System.out.println("Connection incoming ...");

		// Lectura del mensaje que el cliente le envia al servidor.
		String clientMessage = input.readLine();

		System.out.println("From client: " + clientMessage);

		// Procesamiento de la informacion que el cliente ha enviado
		// al servidor.
		String answer = clientMessage;

		System.out.println("Sent to client: " + answer);

		// Envio del mensaje al cliente.
		output.println(answer);
	}
}
