package Ejercicio3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ProtocolClient {

	public static void procesar(BufferedReader input, PrintWriter output) throws IOException {
		String receivedMessage = "";
		while (!receivedMessage.equals("ADIOS")) {
			System.out.println("Escriba el mensaje para enviar: ");
			String message = new Scanner(System.in).nextLine();
			output.println(message);
			receivedMessage = input.readLine();
			System.out.println("El usuario escribi�: " + message);
			System.out.println("Servidor: " + receivedMessage);
		}
	}
}
