package InterfacesDeRed;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class NetworkInterfacesExample {
	public static void main(String[] args) {
		try {
			Enumeration<NetworkInterface> hostNics = NetworkInterface.getNetworkInterfaces();
			while (hostNics.hasMoreElements()) {
				NetworkInterface nic = hostNics.nextElement();
				System.out.println("Interfaz " + nic.getName() + ":");
				Enumeration<InetAddress> nicAddresses = nic.getInetAddresses();
				while (nicAddresses.hasMoreElements()) {
					InetAddress address = nicAddresses.nextElement();
					if (address instanceof Inet4Address) {
						System.out.println("( v4 ) " + address.getHostAddress());
					} else {
						if (address instanceof Inet6Address) {
							System.out.println("( v6 ) " + address.getHostAddress());
						}
					}
				}
			}
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
}