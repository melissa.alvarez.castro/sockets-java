package Ejercicio1;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;

/*
 * EchoTCPServer
 * 
 * El objetivo de este programa es leer de la red un String enviada por un cliente y
 * hacer eco de ese mensaje enviándolo de nuevo al cliente.
 */

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class Servidor {
	public static final int PORT = 3400;

	private ServerSocket listener;
	private Socket serverSideSocket;
	private BufferedReader reader;
	private BufferedInputStream fromNetwork;
	private BufferedOutputStream toFile;
	private String fileName;

	public Servidor() throws Exception {

		System.out.println("SERVIDOR ...");

		listener = new ServerSocket(PORT);

		while (true) {

			System.out.println("Espera de un CLIENTE....");
			serverSideSocket = listener.accept();

			reader = new BufferedReader(new InputStreamReader(serverSideSocket.getInputStream()));
			fileName = reader.readLine();
			fileName = "destino\\" + fileName;

			fromNetwork = new BufferedInputStream(serverSideSocket.getInputStream());
			toFile = new BufferedOutputStream(new FileOutputStream(fileName));

			String sizeString = reader.readLine();
			long size = Long.parseLong(sizeString.split(":")[1]);
			
			byte[] receivedData = new byte[512];
			int in;
			long remainder = size;
			while ((in = fromNetwork.read(receivedData)) != -1) {
				toFile.write(receivedData, 0, in);
				remainder -= in;
				if (remainder == 0)
					break;
			}
			toFile.close();
			reader.close();
		}
	}

	/**
	 * Metodo principal utilizado para lanzar el programa servidor.
	 * 
	 * @throws Exception
	 */
	public static void main(String args[]) throws Exception {
		new Servidor();
	}
}