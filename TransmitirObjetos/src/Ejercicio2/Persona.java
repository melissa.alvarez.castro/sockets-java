package Ejercicio2;

import java.io.Serializable;

public class Persona implements Serializable{
	private String nombre;
	private int edad;
	private double estatura;

	public Persona(String nombre, int edad, double estatura) {
		this.nombre = nombre;
		this.edad = edad;
		this.estatura = estatura;
	}
	
	@Override
	public String toString() {
		return "Persona [nombre = " + nombre + ", edad = " + edad + ", estatura = " + estatura + "]";
	}
	
}
