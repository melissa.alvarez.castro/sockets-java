package Ejercicio1_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ProtocolClient {

	public static void procesar(BufferedReader input, PrintWriter output) throws IOException {
		
		// Lectura del mensaje que el usuario desea enviar al servidor.
		System.out.print("Mensaje: ");
		String message = new Scanner(System.in).nextLine();

		// Envio del mensaje al servidor.
		output.println(message);

		// Lectura del mensaje que el servidor le envia al cliente.
		String receivedMessage = input.readLine();

		// Impresion del mensaje recibido en la consola.
		System.out.println("FROM SERVER: " + receivedMessage);
	}
}
