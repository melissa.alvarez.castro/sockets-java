package Ejercicio1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class ProtocolClient {

	public static void procesar(ObjectInputStream input, ObjectOutputStream output)
			throws IOException, ClassNotFoundException {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Elemento 1");
		list.add("Elemento 2");
		list.add("Elemento 3");
		list.add("Elemento 4");
		list.add("Elemento 5");
		output.writeObject(list);
		output.flush();
		Object object = input.readObject();
		System.out.println("Servidor: " + object);
	}

}
