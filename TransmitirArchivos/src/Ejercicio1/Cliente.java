package Ejercicio1;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;

/*
 * EchoTCPClient
 * 
 * El objetivo de este programa es leer del teclado un String y enviarlo al
 * servidor. El servidor hace eco de este mensaje y lo envia de nuevo al cliente.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Cliente {
	public static final int PORT = 3400;
	public static final String SERVER = "localhost";

	private Socket clientSideSocket;
	private PrintWriter writer;
	private BufferedInputStream fromFile;
	private BufferedOutputStream toNetwork;
	private File localFile;
	private String fileName;

	public Cliente() throws Exception {
		System.out.println("CLIENTE ...");

		clientSideSocket = new Socket(SERVER, PORT);

		fileName = "Documento.pdf";

		writer = new PrintWriter(clientSideSocket.getOutputStream(), true);
		writer.println(fileName);

		localFile = new File(fileName);
		fromFile = new BufferedInputStream(new FileInputStream(localFile));
		toNetwork = new BufferedOutputStream(clientSideSocket.getOutputStream());

		long size = localFile.length();
		writer.println("Size:" + size);

		byte[] byteArray = new byte[512];
		int in;
		while ((in = fromFile.read(byteArray)) != -1) {
			toNetwork.write(byteArray, 0, in);
		}

		toNetwork.flush();
		fromFile.close();
	}

	/**
	 * Metodo principal de la aplicacion.
	 * @throws Exception 
	 */
	public static void main(String args[]) throws Exception {
		new Cliente();
	}
}